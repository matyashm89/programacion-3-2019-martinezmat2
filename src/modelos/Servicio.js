
const mongoose = require('mongoose');
const { Schema } = mongoose;

const servicioSchema = new Schema({
  codigo: String,
  nombre: String,
 
  fechaCreacion: Date,
  fechaActualizacion: Date,
  tarea: [{ type: Schema.ObjectId, ref: "Tarea" }] 
});

mongoose.model('servicios', servicioSchema);
module.exports = mongoose.model("Servicio", servicioSchema);